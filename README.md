# What works so far?

## Progress

### Actors:
  - [X] Manual brightness (deviceinfo sysfs)
  - [X] Torchlight (/sys/devices/platform/flashlights_mt6360)
  - [ ] Vibration (/sys/devices/platform/aw8622)
### Camera: app crash
  - [X] Flashlight
  - [X] Photo
  - [X] Video
  - [X] Switching between cameras
### Cellular: (ofono)
  - [X] Carrier info, signal strength
  - [X] Data connection
  - [X] Dual SIM functionality
  - [ ] Incoming, outgoing calls
  - [ ] MMS in, out
  - [ ] PIN unlock
  - [X] SMS in, out
  - [ ] Change audio routings
  - [ ] Voice in calls
  - [ ] Volume control in calls
### Endurance: can't test
  - [ ] 24+ hours battery lifetime
  - [ ] 7+ days stability
### GPU: (udev)
  - [X] Boot into UI
  - [X] Hardware video playback
### Misc:
  - [X] AppArmor patches (patched)
  - [X] Battery percentage
  - [?] Offline charging
  - [X] Online charging
  - [X] Recovery image (Notes: The Ui take time to show)
  - [X] Reset to factory defaults
  - [X] RTC time
  - [X] SD card storage
  - [X] Shutdown / Reboot
  - [ ] Wireless External monitor
  - [X] Waydroid
  - [X] KVM
### Network:
  - [X] Bluetooth
  - [X] Flight mode
  - [X] Hotspot (no ac wifi though)
  - [ ] NFC (mine don't have)
  - [X] WiFi (devicehacks script for wmtWifi)
  - [ ] FM radio
### Sensors: (deviceinfo)
  - [X] Automatic brightness
  - [X] Fingerprint reader
  - [ ] GPS
  - [X] Proximity
  - [X] Rotation
  - [X] Touchscreen
### Sound: (pulseaudio droid card module)
  - [X] Earphones
  - [X] Loudspeaker
  - [X] Microphone
  - [X] Volume control
### USB: (disable rescue mode)
  - [X] MTP access
  - [X] ADB access

## Caution
### in order to install Ubuntu Touch on device, Android 11 firmware V12.5.16.0.RKLMIXM is required

## Unlock Bootloader
- Unlock using mi tools
  
## Install:
### Put the device in fastboot mode (bootloader)
Hold VOLUME DOWN + POWER BUTTON

<!-- - Reboot to fastbootd
```command
fastboot reboot fastboot
```
- Flash disable verification vbmeta
```command
fastboot --disable-verity --disable-verification flash vbmeta ./vbmeta.img
```
- Format userdata you may require fastboot binary version 30.0.0-6374843
```command
fastboot format:ext4 userdata
```
- Delete product logical partition
```command
fastboot delete-logical-partition product
```
- Flash Ubuntu Rootfs:
```command
fastboot flash system ./ubuntu.img
```
- Flash the Ubuntu boot.img:
```command
fastboot flash boot ./boot.img
``` -->

- Flash Ubuntu boot.img
```command
fastboot flash boot ./boot.img
```
- Flash disable verification vbmeta
```command
fastboot --disable-verity --disable-verification flash vbmeta ./vbmeta.img
```
- Format userdata you may require fastboot binary version 30.0.0-6374843
```command
fastboot format:ext4 userdata
```
- Reboot to recovery
```command
fastboot reboot recovery
```
- Install Ubuntu Rootfs:
```command
adb shell mount /data && adb push ubuntu.img /data
```
